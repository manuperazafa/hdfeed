# HN-feed
Auto-fetch the Hacker News links about your favorite topic, to read later.
## Get started
Install dependencies
```
npm install
bower install
```
config/app.js
Set your MongoDb, https://gitlab.com/manuperazafa/hdfeed/blob/master/config/app.js#L7
```
Now check the API documentation
## API
#### Start the NodeJS server
```
npm run server
```
#### Force an API load
```
npm run load-api
```
#### Clean the feed collection
```
npm run clean-db
```
## License
MIT
